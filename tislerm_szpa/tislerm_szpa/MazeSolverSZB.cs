﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tislerm_szpa
{
    /// <summary>
    /// Labirintus megoldó szélességi bejárás algoritmus segítségével
    /// </summary>
    class MazeSolverSZB
    {
        MazeCell[,] maze;
        int[] d;
        Queue<MazeCell> sor;
        List<MazeCell> halmaz;
        MazeCell honnan;
        MazeCell hova;
        /// <summary>
        /// konstruktor 
        /// </summary>
        /// <param name="maze">megoldandó labirintus</param>
        public MazeSolverSZB(MazeCell[,] maze)
        {
            this.maze = maze;
            this.d = new int[maze.GetLength(0) * maze.GetLength(1)];
            this.sor = new Queue<MazeCell>();
            this.halmaz = new List<MazeCell>();
            this.honnan = maze[1, 1];
            this.hova = CelKivalasztas();
            Console.WriteLine("Honnan y= " + honnan.Index / maze.GetLength(0) + ", x = " + honnan.Index % maze.GetLength(0));
            Console.WriteLine("Hova y= " + hova.Index / maze.GetLength(0) + ", x = " + hova.Index % maze.GetLength(0));
        }
        /// <summary>
        /// szélességi bejárás algoritmus implementációja
        /// </summary>
        /// <returns>a megtalált útvonal hossza</returns>
        public int SzelessegiBejaras()
        {
            halmaz.Add(honnan);
            sor.Enqueue(honnan);
            d[honnan.Index] = 0;
            while (sor.Count != 0)
            {
                MazeCell k = sor.Dequeue();
                List<MazeCell> szomszedok = GetSzomszedok(k);
                foreach (MazeCell mazecell in szomszedok)
                {
                    if (!halmaz.Contains(mazecell))
                    {
                        sor.Enqueue(mazecell);
                        halmaz.Add(mazecell);
                        d[mazecell.Index] = d[k.Index] + 1;
                    }
                }
            }
            InsertRoad();
            return d[hova.Index];
        }
        /// <summary>
        /// egy adott pont szomszédai összegyűjtése
        /// </summary>
        /// <param name="k">adott pont</param>
        /// <returns>lista, benne a szomszédaival</returns>
        List<MazeCell> GetSzomszedok(MazeCell k)
        {
            List<MazeCell> szomszedok = new List<MazeCell>();

            int x = k.Index % maze.GetLength(0);
            int y = k.Index / maze.GetLength(0);

            //felette
            if (y - 1 >= 0 && maze[y - 1,x].Type == Type.hole)
            {
                szomszedok.Add(maze[y - 1, x]);
            }
            //alatta
            if (y + 1 < maze.GetLength(0) && maze[y + 1, x].Type == Type.hole)
            {
                szomszedok.Add(maze[y + 1, x]);
            }
            //balra
            if (x - 1 >= 0 && maze[y, x - 1].Type == Type.hole)
            {
                szomszedok.Add(maze[y, x - 1]);
            }
            //jobbra
            if (x + 1 < maze.GetLength(1) && maze[y, x + 1].Type == Type.hole)
            {
                szomszedok.Add(maze[y, x + 1]);
            }
            return szomszedok;
        }
        /// <summary>
        /// véletlenszerű célkiválasztás
        /// </summary>
        /// <returns>kiválasztott cella</returns>
        MazeCell CelKivalasztas()
        {
            MazeCell temp_hova = new MazeCell(-1);
            while (temp_hova.Index == -1)
            {
                int rnd_x = RandomManager.rnd.Next(maze.GetLength(1));
                int rnd_y = RandomManager.rnd.Next(maze.GetLength(0));
                if (maze[rnd_y, rnd_x].Type == Type.hole && maze[rnd_y, rnd_x] != honnan)
                {
                    temp_hova = maze[rnd_y, rnd_x];
                }
            }
            return temp_hova;
        }
        /// <summary>
        /// megoldott útvonal kirajzolása
        /// </summary>
        void InsertRoad()
        {
            int index = hova.Index;
            int x = index % maze.GetLength(0);
            int y = index / maze.GetLength(0);
            maze[y, x].Type = Type.road;
            MazeCell previous_cell = hova;
            while ((previous_cell = GetPreviousSzomszedok(previous_cell)) != null)
            {
                index = previous_cell.Index;
                x = index % maze.GetLength(0);
                y = index / maze.GetLength(0);
                maze[y, x].Type = Type.road;
            }

        }
        /// <summary>
        /// adott cellába lépés előtti cella megkeresése
        /// </summary>
        /// <param name="k">adott cella</param>
        /// <returns>előző cella</returns>
        MazeCell GetPreviousSzomszedok(MazeCell k)
        {
            int x = k.Index % maze.GetLength(0);
            int y = k.Index / maze.GetLength(0);

            //felette
            if (y - 1 >= 0 && maze[y - 1, x].Type == Type.hole && d[maze[y-1,x].Index] == d[k.Index] - 1)
            {
                return maze[y - 1, x];
            }
            //alatta
            if (y + 1 < maze.GetLength(0) && maze[y + 1, x].Type == Type.hole && d[maze[y + 1, x].Index] == d[k.Index] - 1)
            {
                return maze[y + 1, x];
            }
            //balra
            if (x - 1 >= 0 && maze[y, x - 1].Type == Type.hole && d[maze[y, x - 1].Index] == d[k.Index] - 1)
            {
                return maze[y, x - 1];
            }
            //jobbra
            if (x + 1 < maze.GetLength(1) && maze[y, x + 1].Type == Type.hole && d[maze[y, x + 1].Index] == d[k.Index] - 1)
            {
                return maze[y, x + 1];
            }
            return null;
        }
        /// <summary>
        /// megoldott labirintus kirajzolása
        /// </summary>
        public void DrawMaze()
        {
            for (int i = 0; i < maze.GetLength(0); i++)
            {
                for (int j = 0; j < maze.GetLength(1); j++)
                {

                    if (maze[i, j].Type == Type.wall)
                    {
                        Console.Write('/');
                    }
                    else if (maze[i,j].Type == Type.road)
                    {
                        Console.Write('X');
                    }
                    else
                    {
                        Console.Write(' ');
                    }
                }
                Console.WriteLine();
            }
        }
        /// <summary>
        /// megoldott útvonal hosszának visszaadása
        /// </summary>
        /// <returns>megoldott útvonal hossza</returns>
        public int ReturnRoadLength()
        {
            return d[hova.Index];
        }
    }
}
