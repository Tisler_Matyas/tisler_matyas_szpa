﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace tislerm_szpa
{
    /// <summary>
    /// Szálankénti osztálydefiníció labirintus generáláshoz rekurzív visszalépéses keresés segítségével
    /// </summary>
    class WorkerGeneration
    {
        MazeCell[,] maze;
        int index;
        int intervall;
        int first_index;
        int last_index;
        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="index">szál tömbbeli kezdő indexe</param>
        /// <param name="intervall">a vizsgálandó intervallum a kezdő indextől</param>
        /// <param name="maze">előre inicializált labirintus, amiben a szálak, egymástól függetlenül dolgozni fognak</param>
        public WorkerGeneration(int index, int intervall, MazeCell[,] maze)
        {
            this.maze = maze;
            this.index = index;
            this.intervall = intervall;
            this.first_index = index * intervall;
            this.last_index = (index + 1) * intervall;
        }
        /// <summary>
        /// algoritmus indítása a kezdőpontból
        /// </summary>
        public void StartGeneration()
        {
            //CravePassage(rnd.Next(maze.GetLength(0)-1), rnd.Next(maze.GetLength(1)-1));
            CravePassage(first_index, 0);
        }
        /// <summary>
        /// labirintus generálás implementációja
        /// </summary>
        /// <param name="y">vizsgálandó cella y koordinátája</param>
        /// <param name="x">vizsgálandó cella x koordinátája</param>
        void CravePassage(int y, int x)
        {
            List<char> directions = new List<char> { 'N', 'S', 'E', 'W' };
            List<char> random_directions = new List<char> { };
            while (directions.Count != 0)
            {
                int random_index = RandomManager.rnd.Next(directions.Count);
                char direction = directions[random_index];
                directions.RemoveAt(random_index);
                random_directions.Add(direction);
                //Console.WriteLine(direction);
            }

            for (int i = 0; i < random_directions.Count; i++)
            {
                int temp_i = i;
                if (random_directions[temp_i] == 'N')
                {
                    if ((y >= first_index+2) && maze[y - 2, x].Type == Type.wall)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            int temp_j = j;
                            maze[y - 2 + temp_j, x].Type = Type.hole;
                            DrawMazeCell(y - 2 + temp_j, x);
                        }
                        CravePassage(y - 2, x);
                    }
                }
                else if (random_directions[temp_i] == 'S')
                {
                    if ((y + 2) < last_index && maze[y + 2, x].Type == Type.wall)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            int temp_j = j;
                            maze[y + temp_j, x].Type = Type.hole;
                            DrawMazeCell(y + temp_j, x);
                        }
                        CravePassage(y + 2, x);
                    }
                }
                else if (random_directions[temp_i] == 'W')
                {
                    if ((x >= 2) && (maze[y, x - 2].Type == Type.wall))
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            int temp_j = j;
                            maze[y, x - 2 + temp_j].Type = Type.hole;
                            DrawMazeCell(y, x - 2 + temp_j);
                        }
                        CravePassage(y, x - 2);
                    }
                }
                else if (random_directions[temp_i] == 'E')
                {
                    if ((x + 2) < maze.GetLength(1) && maze[y, x + 2].Type == Type.wall)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            int temp_j = j;
                            maze[y, x + temp_j].Type = Type.hole;
                            DrawMazeCell(y, x + temp_j);
                        }
                        CravePassage(y, x + 2);
                    }
                }
                else
                {
                    //semmi
                }
            }
        }


        
        /// <summary>
        /// egy adott cella kirajzolása
        /// </summary>
        /// <param name="y">kirajzolandó cella y koordinátája</param>
        /// <param name="x">kirajzolandó cella x koordinátája</param>
        void DrawMazeCell(int y, int x)
        {
            //Console.SetCursorPosition(x, y);
            //Console.Write(' ');
            //Thread.Sleep(50);
        }
    }
}
