﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace tislerm_szpa
{
    /// <summary>
    /// Labirintus generálás HuntAndKill algoritmus segítségével
    /// </summary>
    class MazeGenerationHAK
    {
        MazeCell[,] maze;
        int new_y;
        int new_x;
        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="height">generált labirintus magassága</param>
        /// <param name="width">generált labirintus szélessége</param>
        /// <param name="maze">lgenerált labirintus</param>
        /// <param name="new_y">kezdőpont y koordinátája</param>
        /// <param name="new_x">kezdőpont x koordinátája</param>
        public MazeGenerationHAK(int height, int width, MazeCell[,] maze, int new_y, int new_x)
        {
            this.maze = maze;
            this.new_y = new_y;
            this.new_x = new_x;
        }
        /// <summary>
        /// megadott pontból következő pont keresése az úthoz 
        /// </summary>
        /// <param name="y">megadott pont y koordinátája</param>
        /// <param name="x">megadott pont x koordinátája</param>
        /// <returns></returns>
        bool Walk(int y, int x)
        {
            List<char> directions = new List<char> { 'N', 'S', 'E', 'W' };
            List<char> random_directions = new List<char> { };
            while (directions.Count != 0)
            {
                int random_index = RandomManager.rnd.Next(directions.Count);
                char direction = directions[random_index];
                directions.RemoveAt(random_index);
                random_directions.Add(direction);
                //Console.WriteLine(direction);
            }

            for (int i = 0; i < random_directions.Count; i++)
            {
                if (random_directions[i] == 'N')
                {
                    if ((y >= 2) && maze[y - 2, x].Type == Type.wall)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            maze[y - 2 + j, x].Type = Type.hole;
                            DrawMazeCell(y - 2 + j, x);
                        }
                        new_y = y - 2;
                        new_x = x;
                        return true;
                    }
                }
                else if (random_directions[i] == 'S')
                {
                    if ((y + 2) < maze.GetLength(0) && maze[y + 2, x].Type == Type.wall)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            maze[y + j, x].Type = Type.hole;
                            DrawMazeCell(y + j, x);
                        }
                        new_y = y + 2;
                        new_x = x;
                        return true;
                    }
                }
                else if (random_directions[i] == 'W')
                {
                    if ((x >= 2) && (maze[y, x - 2].Type == Type.wall))
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            maze[y, x - 2 + j].Type = Type.hole;
                            DrawMazeCell(y, x - 2 + j);
                        }
                        new_y = y;
                        new_x = x - 2;
                        return true;
                    }
                }
                else if (random_directions[i] == 'E')
                {
                    if ((x + 2) < maze.GetLength(1) && maze[y, x + 2].Type == Type.wall)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            maze[y, x + j].Type = Type.hole;
                            DrawMazeCell(y, x + j);
                        }
                        new_y = y;
                        new_x = x + 2;
                        return true;
                    }
                }
                else
                {
                    //semmi
                }
            }
            return false; //nem tudtunk innen sehova se lépni
        }
        /// <summary>
        /// teljes algoritmus irányítása a helyes sorrendben
        /// </summary>
        public void HuntAndKill()
        {
            Walking();
            while (Hunt())
            {
                Walking();
            }
        }
        /// <summary>
        /// labirintus egy útjának generálása
        /// </summary>
        void Walking()
        {
            bool successful_walk = Walk(new_y, new_x);
            while (successful_walk)
            {
                successful_walk = Walk(new_y, new_x);
            }
        }
        /// <summary>
        /// újabb út indításához szükséges kezdőpont keresése
        /// </summary>
        /// <returns>true - találtunk egy megfelelő pontot, false - ha nincs megfelelő pont a labirintusban az útgeneráláshoz</returns>
        bool Hunt()
        {
            bool successfull_search = false;
            int i = 0;
            while (i < maze.GetLength(0) && !successfull_search)
            {
                int j = 0;
                while (j < maze.GetLength(1) && !successfull_search)
                {
                    successfull_search = CheckCellIsUnvisited(i, j);
                    j++;
                }
                i++;
            }
            return successfull_search;
        }
        /// <summary>
        /// generált labirintus kirajzolása
        /// </summary>
        public void DrawMaze()
        {
            for (int i = 0; i < maze.GetLength(0); i++)
            {
                for (int j = 0; j < maze.GetLength(1); j++)
                {

                    if (maze[i, j].Type == Type.wall)
                    {
                        Console.Write('X');
                    }
                    else
                    {
                        Console.Write(' ');
                    }
                }
                Console.WriteLine();
            }
        }
        /// <summary>
        /// adott cella kirajzolása
        /// </summary>
        /// <param name="y">adott cella y koordinátája</param>
        /// <param name="x">adott cella x koordinátája</param>
        void DrawMazeCell(int y, int x)
        {
            //Console.SetCursorPosition(x, y);
            //Console.Write(' ');
            //Thread.Sleep(20);
        }
        /// <summary>
        /// egy adott pont megvizsgálata, hogy megfelelő-e útgenerálási kezdőponthoz
        /// </summary>
        /// <param name="y">adott pont y koordinátája</param>
        /// <param name="x">adott pont x koordinátája</param>
        /// <returns></returns>
        bool CheckCellIsUnvisited(int y, int x)
        {
            bool unvisited = true;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (y + i >= 0 && x + j >= 0 && y + i < maze.GetLength(0) && x + j < maze.GetLength(1))
                    {
                        if (y != i || x != j)
                        {
                            if (maze[y + i,x + j].Type != Type.wall)
                            {
                                unvisited = false;                            
                            }
                        }
                    }
                }
            }
            if (unvisited == true)
            {
                if ((y >= 2) && maze[y - 2, x].Type == Type.hole)
                {
                    new_y = y - 2;
                    new_x = x;                 
                }
                else if ((y + 2) < maze.GetLength(0) && maze[y + 2, x].Type == Type.hole)
                {
                    new_y = y + 2;
                    new_x = x;
                }
                else if ((x >= 2) && (maze[y, x - 2].Type == Type.hole))
                {
                    new_y = y;
                    new_x = x - 2;
                }
                else if ((x + 2) < maze.GetLength(1) && maze[y, x + 2].Type == Type.hole)
                {
                    new_y = y;
                    new_x = x + 2;
                }
                else
                {
                    unvisited = false;
                }
            }

            return unvisited;
        }
    }
}
