﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tislerm_szpa
{
    /// <summary>
    /// Párhuzamos rekurzív visszalépéses keresés algoritmus implementációja a labirintus generáláshoz
    /// </summary>
    class MasterGeneration
    {
        int THREADS = Environment.ProcessorCount;
        int MAZESIZE;
        List<MazeGenerationRB> workers;
        List<Task> generator_tasks;
        MazeCell[,] maze;
        int intervall;
        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="MAZESIZE">létrehozandó labirintus sorának és oszlopának mérete</param>
        public MasterGeneration(int MAZESIZE)
        {
            this.MAZESIZE = MAZESIZE;
            this.workers = new List<MazeGenerationRB>();
            this.generator_tasks = new List<Task>();
            this.intervall = MAZESIZE / THREADS;
            this.maze = new MazeCell[MAZESIZE, MAZESIZE];
            for (int i = 0; i < maze.GetLength(0); i++)
            {
                for (int j = 0; j < maze.GetLength(1); j++)
                {
                    maze[i, j] = new MazeCell(i * MAZESIZE + j);
                }
            }
           //DrawMaze();
        }
        /// <summary>
        /// Labirintus generálás 1 szállal, így valójában szekvenciális
        /// </summary>
        public void SeqRunning()
        {
            this.workers.Add(new MazeGenerationRB(MAZESIZE,MAZESIZE));
            foreach (MazeGenerationRB worker in workers)
            {
                generator_tasks.Add(new Task(() =>
                {
                    worker.StartGeneration();
                }, TaskCreationOptions.LongRunning));
            }

            foreach (Task t in generator_tasks)
            {
                t.Start();
            }

            Task.WaitAll(generator_tasks.ToArray());
        }
        /// <summary>
        /// Labirintus generálás párhuzamosan, az elérhető szálak arányában
        /// </summary>
        public void ParRunning()
        {
            for (int i = 0; i < THREADS; i++)
            {
                this.workers.Add(new MazeGenerationRB(intervall, MAZESIZE));
            }
            foreach (MazeGenerationRB worker in workers)
            {
                generator_tasks.Add(new Task(() =>
                {
                    worker.StartGeneration();
                }, TaskCreationOptions.LongRunning));
            }

            foreach (Task t in generator_tasks)
            {
                t.Start();
            }

            Task.WaitAll(generator_tasks.ToArray());
            for (int i = 0; i < workers.Count; i++)
            {
                MazeCell[,] temp_maze= workers[i].GetMaze();
                for (int y = 0; y < temp_maze.GetLength(0); y++)
                {
                    for (int x = 0; x < temp_maze.GetLength(1); x++)
                    {
                        this.maze[i * intervall + y, x] = temp_maze[y, x];
                    }
                }
            }
            //DrawMaze();
        }

        /// <summary>
        /// generált labirintus kirajzolása
        /// </summary>
        public void DrawMaze()
        {
            for (int i = 0; i < maze.GetLength(0); i++)
            {
                for (int j = 0; j < maze.GetLength(1); j++)
                {

                    if (maze[i, j].Type == Type.wall)
                    {
                        Console.Write('X');
                    }
                    else
                    {
                        Console.Write(' ');
                    }
                }
                Console.WriteLine();
            }
        }
    }

}
