﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace tislerm_szpa
{
    /// <summary>
    /// egy pontból lehetséges lépések iránya - észak, dél, nyugat, kelet
    /// </summary>
    enum Direction { N, S, E, W}
    /// <summary>
    /// Labirintus generálás algoritmus implementációja rekurzív visszalépéses keresés segítségével
    /// </summary>
    class MazeGenerationRB
    {
        static Random rnd = new Random();
        MazeCell[,] maze;
        int height;
        int width;
        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="height">a létrehozandó labirintus magassága</param>
        /// <param name="width">a létrehozandó labirintus szélessége</param>
        public MazeGenerationRB(int height, int width)
        {
            this.height = height;
            this.width = width;
            this.maze = new MazeCell[height, width];
            for (int i = 0; i < maze.GetLength(0); i++)
            {
                for (int j = 0; j < maze.GetLength(1); j++)
                {
                    maze[i, j] = new MazeCell(i * width + j);
                }
            }
        
        }
        /// <summary>
        /// Generálás indítása
        /// </summary>
        public void StartGeneration()
        {
            CravePassage(1, 1);
        }
        /// <summary>
        /// Labrintus generálás implementációja (rekurzív visszalépéses keresés algoritmussal)
        /// </summary>
        /// <param name="y">vizsgálandó pont y koordinátája</param>
        /// <param name="x">vizsgálandó pont x koordinátája</param>
        void CravePassage(int y, int x)
        {
            List<char> directions = new List<char> { 'N','S','E','W'};
            List<char> random_directions = new List<char> {};
            while (directions.Count != 0)
            {
                int random_index = rnd.Next(directions.Count);
                char direction = directions[random_index];
                directions.RemoveAt(random_index);
                random_directions.Add(direction);
                //Console.WriteLine(direction);
            }

            for (int i = 0; i < random_directions.Count; i++)
            {
                if (random_directions[i] == 'N')
                {
                    if ((y >= 2) && maze[y-2,x].Type == Type.wall)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            maze[y - 2 + j, x].Type = Type.hole;
                            DrawMazeCell(y - 2 + j, x);
                        }
                        CravePassage(y - 2, x);
                    }
                }
                else if (random_directions[i] == 'S')
                {
                    if ((y + 2) < maze.GetLength(0) && maze[y+2,x].Type == Type.wall)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            maze[y + j, x].Type = Type.hole;
                            DrawMazeCell(y + j, x);
                        }
                        CravePassage(y + 2, x);
                    }
                }
                else if (random_directions[i] == 'W')
                {
                    if ((x >= 2) && (maze[y,x-2].Type == Type.wall))
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            maze[y, x - 2 + j].Type = Type.hole;
                            DrawMazeCell(y, x - 2 + j);
                        }
                        CravePassage(y, x - 2);
                    }
                }
                else if (random_directions[i] == 'E')
                {
                    if ((x+2) < maze.GetLength(1) && maze[y,x+2].Type == Type.wall)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            maze[y, x + j].Type = Type.hole;
                            DrawMazeCell(y, x + j);
                        }
                        CravePassage(y, x + 2);
                    }
                }
                else
                {
                    //semmi
                }
            }
        }

        /// <summary>
        /// generált labirintus kirajzolása
        /// </summary>
        public void DrawMaze()
        {
            for (int i = 0; i < maze.GetLength(0); i++)
            {
                for (int j = 0; j < maze.GetLength(1); j++)
                {

                    if (maze[i, j].Type == Type.wall)
                    {
                        Console.Write('X');
                    }
                    else
                    {
                        Console.Write(' ');
                    }
                }
                Console.WriteLine();
            }
        }
        /// <summary>
        /// egy megadott cella kirajzolása (lépésről lépésre történő kirajzolás esetén)
        /// </summary>
        /// <param name="y">kirajzolandó cella y koordinátája</param>
        /// <param name="x">kirajzolandó cella x koordinátája</param>
        void DrawMazeCell(int y, int x)
        {
            //Console.SetCursorPosition(x, y);
            //Console.Write(' ');
            //Thread.Sleep(20);
        }
        /// <summary>
        /// generált labirintus visszaadása
        /// </summary>
        /// <returns></returns>
        public MazeCell[,] GetMaze()
        {
            return maze;
        }
    }
}
