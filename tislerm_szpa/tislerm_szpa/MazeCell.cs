﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tislerm_szpa
{
    /// <summary>
    /// Egy labirintus cella lehetséges típusai
    /// </summary>
    enum Type {wall, hole, road}
    /// <summary>
    /// Labirintus cella 
    /// </summary>
    class MazeCell
    {
        Type type;
        int index;
        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="index">a cella indexe</param>
        public MazeCell(int index)
        {
            this.Type = Type.wall;
            this.index = index;
        }
        /// <summary>
        /// type olvasható és írható tulajdonsága
        /// </summary>
        public Type Type { get => type; set => type = value; }
        /// <summary>
        /// index ovlasható és írható tulajdonsága
        /// </summary>
        public int Index { get => index; set => index = value; }
    }
}
