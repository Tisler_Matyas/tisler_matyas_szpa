﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace tislerm_szpa
{
    class Program
    {
        static void Main(string[] args)
        {
            int MAZESIZE = 225;
            //RecursiveBacktrackGenerationSequence(MAZESIZE, true);
            //RecursiveBacktrackGenerationParallel(MAZESIZE, true);
            //HuntAndKillGenerationSequence(MAZESIZE, true);
            //HuntAndKillGenerationParallel(MAZESIZE, true);
            MazeCell[,] maze = HuntAndKillGenerationSequence(MAZESIZE, false);
            //SZBSolvingSequence(MAZESIZE, maze, true);
            SZBSolvingParallel(MAZESIZE,maze, false);
            //GenerateDefaultMazeSequence(MAZESIZE,  true);
            //GenerateDefaultMazeParallel(MAZESIZE,  true);

            Console.ReadLine();

        }
        /// <summary>
        /// Szekvenciális rekurzív visszalépéses keresés algoritmus indítása a labirintus generáláshoz
        /// </summary>
        /// <param name="MAZESIZE">A létrehozandó labirintus oszlopának és sorának mérete</param>
        /// <param name="DrawMaze">True - esetén kirajzoljuk a generált labirintust, false - esetén nem rajzoljuk ki a generált labirintust</param>
        static void RecursiveBacktrackGenerationSequence(int MAZESIZE, bool DrawMaze)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            MazeGenerationRB mazeg = new MazeGenerationRB(MAZESIZE, MAZESIZE);
            mazeg.StartGeneration();
            sw.Stop();
            Console.WriteLine("Labirintus méret: " + MAZESIZE);
            Console.WriteLine("RecursiveBacktrack szekvenciális futtatás eltelt idő: " + sw.Elapsed.TotalSeconds + " másodperc");

            if (DrawMaze)
            {
                mazeg.DrawMaze();
            }
        }
        /// <summary>
        /// Párhuzamos rekurzív visszalépéses keresés algoritmus indítása a labirintus generáláshoz
        /// </summary>
        /// <param name="MAZESIZE">A létrehozandó labirintus oszlopának és sorának mérete</param>
        /// <param name="DrawMaze">True - esetén kirajzoljuk a generált labirintust, false - esetén nem rajzoljuk ki a generált labirintust</param>
        static void RecursiveBacktrackGenerationParallel(int MAZESIZE, bool DrawMaze)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            MasterGeneration master = new MasterGeneration(MAZESIZE);
            master.ParRunning();
            sw.Stop();
            Console.WriteLine("Labirintus méret: " + MAZESIZE);
            Console.WriteLine("RecursiveBacktrack párhuzamos futtatás eltelt idő: " + sw.Elapsed.TotalSeconds + " másodperc");
            if (DrawMaze)
            {
                master.DrawMaze();
            }
        }
        /// <summary>
        /// Szekvenciális HuntAndKill algoritmus indítása a labirintus generáláshoz
        /// </summary>
        /// <param name="MAZESIZE">A létrehozandó labirintus oszlopának és sorának mérete</param>
        /// <param name="DrawMaze">True - esetén kirajzoljuk a generált labirintust, false - esetén nem rajzoljuk ki a generált labirintust</param>
        /// <returns>a generált labirintus</returns>
        static MazeCell[,] HuntAndKillGenerationSequence(int MAZESIZE, bool DrawMaze)
        {
            MazeCell[,] maze = new MazeCell[MAZESIZE, MAZESIZE];
            for (int i = 0; i < maze.GetLength(0); i++)
            {
                for (int j = 0; j < maze.GetLength(1); j++)
                {
                    maze[i, j] = new MazeCell(i * MAZESIZE + j);
                }
            }
            Stopwatch sw = new Stopwatch();
            sw.Start();
            MazeGenerationHAK mazeGenerationHAK = new MazeGenerationHAK(MAZESIZE, MAZESIZE, maze, 1, 1);
            mazeGenerationHAK.HuntAndKill();
            sw.Stop();
            Console.WriteLine("Labirintus méret: " + MAZESIZE);
            Console.WriteLine("HuntAndKill szekvenciális futtatás eltelt idő: " + sw.Elapsed.TotalSeconds);
            if (DrawMaze)
            {
                mazeGenerationHAK.DrawMaze();
            }
            return maze;
        }
        /// <summary>
        /// Párhuzamos HuntAndKill algoritmus indítása a labirintus generáláshoz
        /// </summary>
        /// <param name="MAZESIZE">A létrehozandó labirintus oszlopának és sorának mérete</param>
        /// <param name="DrawMaze">True - esetén kirajzoljuk a generált labirintust, false - esetén nem rajzoljuk ki a generált labirintust</param>
        /// <returns>a generált labirintus</returns>
        static MazeCell[,] HuntAndKillGenerationParallel(int MAZESIZE, bool DrawMaze)
        {
            MazeCell[,] maze = new MazeCell[MAZESIZE, MAZESIZE];
            for (int i = 0; i < maze.GetLength(0); i++)
            {
                for (int j = 0; j < maze.GetLength(1); j++)
                {
                    maze[i, j] = new MazeCell(i * MAZESIZE + j);
                }
            }         
            int THREADS = Environment.ProcessorCount;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            List<Task> tasks_HAKP = new List<Task>();
            List<MazeGenerationHAKP> workers = new List<MazeGenerationHAKP>();
            for (int i = 0; i < THREADS; i++)
            {
                workers.Add(new MazeGenerationHAKP(maze, 1, 1));
            }
            foreach (MazeGenerationHAKP worker in workers)
            {
                tasks_HAKP.Add(new Task(() =>
                {
                    worker.HuntAndKill();
                }, TaskCreationOptions.LongRunning));
            }
            foreach (Task task in tasks_HAKP)
            {
                task.Start();
                Thread.Sleep(100);
            }
            Task.WaitAll(tasks_HAKP.ToArray());
            sw.Stop();
            Console.WriteLine("Labirintus méret: " + MAZESIZE);
            Console.WriteLine("HuntAndKill párhuzamos futtatás eltelt idő: " + sw.Elapsed.TotalSeconds);
            if (DrawMaze)
            {
                workers[0].DrawMaze();
            }
            return maze;
        }
        /// <summary>
        /// Szekvenciális szélességi bejárás algoritmus indítása a labirintus megoldásához
        /// </summary>
        /// <param name="MAZESIZE">A létrehozott labirintus oszlopának és sorának mérete</param>
        /// <param name="maze">A generált labirintus</param>
        /// <param name="DrawMaze">True - esetén kirajzoljuk a megoldott labirintust, false - esetén nem rajzoljuk ki a megoldott labirintust</param>
        static void SZBSolvingSequence(int MAZESIZE,MazeCell[,] maze, bool DrawMaze)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            MazeSolverSZB solver = new MazeSolverSZB(maze);
            solver.SzelessegiBejaras();
            sw.Stop();
            Console.WriteLine("Szélességi bejárás szekvenciális futtatás eltelt idő: " + sw.Elapsed.TotalSeconds);
            Console.WriteLine("Lépések száma: " + solver.ReturnRoadLength());
            if (DrawMaze)
            {
                solver.DrawMaze();
            }
        }
        /// <summary>
        /// Párhuzamos szélességi bejárás algoritmus indítása a labirintus megoldásához
        /// </summary>
        /// <param name="MAZESIZE">A létrehozott labirintus oszlopának és sorának mérete</param>
        /// <param name="maze">A generált labirintus</param>
        /// <param name="DrawMaze">True - esetén kirajzoljuk a megoldott labirintust, false - esetén nem rajzoljuk ki a megoldott labirintust</param>
        static void SZBSolvingParallel(int MAZESIZE,MazeCell[,] maze, bool DrawMaze)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            MazeSolverSZBParallel solver = new MazeSolverSZBParallel(maze);
            solver.SzelessegiBejaras();
            sw.Stop();
            Console.WriteLine("Szélességi bejárás párhuzamos futtatás eltelt idő: " + sw.Elapsed.TotalSeconds);
            Console.WriteLine("Lépések száma: " + solver.ReturnRoadLength());
            if (DrawMaze)
            {
                solver.DrawMaze();
            }
        }
        /// <summary>
        /// Labirintus inicializálása szekvenciálisan
        /// </summary>
        /// <param name="MAZESIZE">A létrehozandó labirintus oszlopának és sorának mérete</param>
        /// <param name="DrawMaze">True - esetén kirajzoljuk a megoldott labirintust, false - esetén nem rajzoljuk ki a megoldott labirintust</param>
        /// <returns>az inicializált labirintus</returns>
        static MazeCell[,] GenerateDefaultMazeSequence(int MAZESIZE, bool DrawMaze)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            MazeCell[,] maze = new MazeCell[MAZESIZE, MAZESIZE];
            for (int i = 0; i < maze.GetLength(0); i++)
            {
                for (int j = 0; j < maze.GetLength(1); j++)
                {
                    maze[i, j] = new MazeCell(i * MAZESIZE + j);
                }
            }
            sw.Stop();
            Console.WriteLine("Labirintus méret: " + MAZESIZE);
            Console.WriteLine("Alapértelmezett labirintus szekvenciális futtatás eltelt idő: " + sw.Elapsed.TotalSeconds);
            if (DrawMaze)
            {
                for (int i = 0; i < maze.GetLength(0); i++)
                {
                    for (int j = 0; j < maze.GetLength(1); j++)
                    {

                        if (maze[i, j].Type == Type.wall)
                        {
                            Console.Write('X');
                        }
                        else
                        {
                            Console.Write(' ');
                        }
                    }
                    Console.WriteLine();
                }
            }
            return maze;
        }
        /// <summary>
        /// Labirintus inicializálása párhuzamosan
        /// </summary>
        /// <param name="MAZESIZE">A lérehozandó labirintus oszlopának és sorának mérete</param>
        /// <param name="DrawMaze">True - esetén kirajzoljuk a megoldott labirintust, false - esetén nem rajzoljuk ki a megoldott labirintust</param>
        /// <returns>az inicializált labirintus</returns>
        static MazeCell[,] GenerateDefaultMazeParallel(int MAZESIZE, bool DrawMaze)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            MazeCell[,] maze = new MazeCell[MAZESIZE, MAZESIZE];
            Parallel.For(0, maze.GetLength(0), i =>
             {
                 Parallel.For(0, maze.GetLength(1), j =>
                 {
                     maze[i, j] = new MazeCell(i * MAZESIZE + j);
                 });
             });
            sw.Stop();
            Console.WriteLine("Labirintus méret: " + MAZESIZE);
            Console.WriteLine("Alapértelmezett labirintus párhuzamos futtatás eltelt idő: " + sw.Elapsed.TotalSeconds);
            if (DrawMaze)
            {
                for (int i = 0; i < maze.GetLength(0); i++)
                {
                    for (int j = 0; j < maze.GetLength(1); j++)
                    {

                        if (maze[i, j].Type == Type.wall)
                        {
                            Console.Write('X');
                        }
                        else
                        {
                            Console.Write(' ');
                        }
                    }
                    Console.WriteLine();
                }
            }
            return maze;
        }
    }
}
